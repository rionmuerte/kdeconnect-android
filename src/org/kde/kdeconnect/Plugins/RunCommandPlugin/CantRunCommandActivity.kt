package org.kde.kdeconnect.Plugins.RunCommandPlugin

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.view.Window
import org.kde.kdeconnect.UserInterface.ThemeUtil
import org.kde.kdeconnect_tp.R
import org.kde.kdeconnect_tp.databinding.WidgetRemoteCommandPluginDialogBinding

const val COMMAND_NOT_LISTED = "org.kde.kdeconnect.Plugins.RunCommandPlugin.COMMAND_NOT_LISTED"

class CantRunCommandActivity: Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ThemeUtil.setUserPreferredTheme(this)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        val binding = WidgetRemoteCommandPluginDialogBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.notReachableMessage.visibility = View.VISIBLE;
        if(intent.getBooleanExtra(COMMAND_NOT_LISTED, false)) {
            binding.notReachableMessage.text = getString(R.string.command_unlisted)
        }
        binding.root.setOnClickListener { finish() }
    }
}