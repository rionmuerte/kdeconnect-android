package org.kde.kdeconnect.Plugins.RunCommandPlugin

import android.app.Activity
import android.appwidget.AppWidgetManager
import android.content.Intent
import android.content.res.TypedArray
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import android.widget.RadioButton
import org.kde.kdeconnect.BackgroundService
import org.kde.kdeconnect.UserInterface.List.ListAdapter
import org.kde.kdeconnect_tp.R
import org.kde.kdeconnect_tp.databinding.WidgetRemoteCommandPluginDialogBinding
import org.kde.kdeconnect_tp.databinding.WidgetSingleCommandIconSelectBinding

class RunSingleCommandWidgetConfigureActivity : Activity() {
    private val GET_IMAGE_CODE: Int = 1
    private var selectedImageIndex: Int = 0
    private lateinit var binding: WidgetRemoteCommandPluginDialogBinding
    private lateinit var selector: WidgetSingleCommandIconSelectBinding
    private lateinit var icons: TypedArray
    private lateinit var deviceId: String
    private lateinit var command: String
    private var imgUri : Uri = Uri.EMPTY

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setResult(RESULT_CANCELED);
        binding = WidgetRemoteCommandPluginDialogBinding.inflate(layoutInflater)
        setContentView(binding.root)
        BackgroundService.RunCommand(this) { service: BackgroundService ->
            runOnUiThread {
                val deviceItems = service.devices.values
                        .asSequence()
                        .filter { it.isPaired && it.isReachable }
                        .map { CommandEntry(it.name, null, it.deviceId) }
                        .asIterable()
                        .sortedBy { it.name }
                if (deviceItems.isEmpty()) binding.notReachableMessage.visibility = View.VISIBLE;
                val adapter = ListAdapter(this@RunSingleCommandWidgetConfigureActivity, deviceItems)
                binding.runCommandsDeviceList.adapter = adapter
                binding.runCommandsDeviceList.setOnItemClickListener { _, _, i:Int, _ ->
                    val entry = deviceItems[i]
                    deviceId = entry.key
                    replaceEntriesWithCommands()
                }
            }
        }
    }

    private fun replaceEntriesWithCommands() {
        val commands = BackgroundService
                .getInstance()
                .getDevice(deviceId)
                .getPlugin(RunCommandPlugin::class.java)
                ?.commandItems!!
        if (commands.size < 1) {
            val name = getString(R.string.not_enough_command_entries)
            binding.notReachableMessage.text = name
            binding.notReachableMessage.visibility = View.VISIBLE
            binding.runCommandsDeviceList.visibility = View.GONE
            return
        }
        val adapter = ListAdapter(this, commands)
        binding.runCommandsDeviceList.adapter = adapter
        binding.runCommandsDeviceList.setOnItemClickListener{ _, _, i:Int, _ ->
            command = commands[i].key
            replaceWithIconSelector()
        }
    }

    private fun replaceWithIconSelector() {
        selector = WidgetSingleCommandIconSelectBinding.inflate(layoutInflater)
        icons = resources.obtainTypedArray(R.array.sample_widget_icons)
        setContentView(selector.root)
        selector.radioPresetIcon.setOnClickListener(this::onRadioButtonClicked)
        selector.radioCustomIcon.setOnClickListener(this::onRadioButtonClicked)
        selector.prevIconButton.setOnClickListener(this::onArrowClicked)
        selector.nextIconButton.setOnClickListener(this::onArrowClicked)
        selector.selectCustomIconButton.setOnClickListener(this::onClickSelectCustomIcon)
        selector.proceedButton.setOnClickListener(this::onClickProceed)
    }

    private fun onClickSelectCustomIcon(view: View) {
        val intent = Intent(Intent.ACTION_GET_CONTENT).apply {
            type = "image/*"
            addCategory(Intent.CATEGORY_OPENABLE)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                action = Intent.ACTION_OPEN_DOCUMENT
                addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION)
            }
        }
        startActivityForResult(intent, GET_IMAGE_CODE)
    }

    private fun onClickProceed(view: View) {
        val widgetId = intent?.extras?.getInt(
                AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID) ?: AppWidgetManager.INVALID_APPWIDGET_ID
        if(selector.radioPresetIcon.isChecked) {
            saveWidget(this, widgetId, command, deviceId, selectedImageIndex.toString())
        } else {
            saveWidget(this, widgetId, command, deviceId, imgUri.toString(), true)
        }
        val initializeIntent = Intent().apply {
            setClass(this@RunSingleCommandWidgetConfigureActivity, SingleCommandWidget::class.java)
            action = ACTION_INITIALIZE
            data = Uri.parse(this.toUri(Intent.URI_INTENT_SCHEME))
            putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId)
        }
        sendBroadcast(initializeIntent)
        val resultValue = Intent().apply {
            putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId)
        }
        setResult(RESULT_OK, resultValue)
        finish()
    }

    private fun onRadioButtonClicked(view: View) {
        if(view is RadioButton)
            when (view.id) {
                selector.radioPresetIcon.id -> {
                    selector.radioCustomIcon.isChecked = false
                    selector.proceedButton.isClickable = true
                    return
                }
                selector.radioCustomIcon.id -> {
                    selector.radioPresetIcon.isChecked = false
                    if (imgUri == Uri.EMPTY) selector.proceedButton.isClickable = false
                    return
                }
            }
    }

    private fun onArrowClicked(view: View) {
        if(view is ImageButton) {
            when (view.id) {
                selector.nextIconButton.id ->
                    if (selectedImageIndex < icons.length() - 1) selectedImageIndex++
                selector.prevIconButton.id ->
                    if (selectedImageIndex > 0) selectedImageIndex--
            }
            selector.radioPresetIcon.isChecked = true
            selector.radioCustomIcon.isChecked = false
            var img = icons.getResourceId(selectedImageIndex, 0)
            selector.presetIcon.setImageResource(img)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == RESULT_CANCELED) return
        if(requestCode == GET_IMAGE_CODE && resultCode == RESULT_OK) {
            selector.radioPresetIcon.isChecked = false
            selector.radioCustomIcon.isChecked = true
            selector.proceedButton.isClickable = true
            imgUri = data!!.data!!
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                contentResolver.takePersistableUriPermission(imgUri, Intent.FLAG_GRANT_READ_URI_PERMISSION)
            }
            selector.customIcon.visibility = View.VISIBLE;
            selector.customIcon.setImageURI(imgUri)
        }
    }
}