package org.kde.kdeconnect.Plugins.RunCommandPlugin;

import android.content.Context
import android.content.SharedPreferences
import org.json.JSONObject

class WidgetData(val commandId: String, val deviceId: String, val isImg: Boolean, val img: String)

private const val PREFERENCES_FILE_NAME = "org.kde.kdeconnect.Plugins.RunCommandPlugin.WidgetDataStore.preferences"
private const val DEFAULT_FILE_MODE = 0
private const val ITEM_PREFIX = "run_single_command_widget_"
private const val COMMAND_ID = "commandId"
private const val DEVICE_ID = "deviceId"
private const val IS_IMG = "isImg"
private const val IMG_URI = "imgUri"

private fun getPreferences(context: Context): SharedPreferences {
    return context.getSharedPreferences(PREFERENCES_FILE_NAME, DEFAULT_FILE_MODE)
}

fun readWidget(context: Context, id: Int): WidgetData? {
    val prefs = getPreferences(context)
    val item = ITEM_PREFIX +id.toString()
    if(!prefs.all.containsKey(item)) return null
    val parsed = prefs.getString(item, "")
    return JSONObject(parsed!!).let {
        WidgetData(
                it.getString(COMMAND_ID),
                it.getString(DEVICE_ID),
                it.getBoolean(IS_IMG),
                it.getString(IMG_URI)
        )
    }
}

fun saveWidget(context: Context, id: Int, commandId: String, deviceId: String, imgUri: String, isImg : Boolean = false) {
    getPreferences(context).edit().run {
        val name = ITEM_PREFIX + id.toString()
        val data = JSONObject(mapOf(
                COMMAND_ID to commandId,
                DEVICE_ID to deviceId,
                IS_IMG to isImg,
                IMG_URI to imgUri
                )).toString()
        putString(name, data)
        apply()
    }
}

fun deleteWidget(context: Context, id:Int) {
    getPreferences(context).edit().runCatching {
        val name = ITEM_PREFIX + id.toString()
        remove(name)
        apply()
    }
}