package org.kde.kdeconnect.Plugins.RunCommandPlugin

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.util.Log
import android.widget.RemoteViews
import org.kde.kdeconnect.BackgroundService
import org.kde.kdeconnect_tp.R

const val ACTION_INITIALIZE = "org.kde.kdeconnect.Plugins.RunCommandPlugin.ACTION_INITIALIZE"
private const val ACTION_EXECUTE = "org.kde.kdeconnect.Plugins.RunCommandPlugin.ACTION_EXECUTE"


class SingleCommandWidget : AppWidgetProvider() {
    override fun onReceive(context: Context, intent: Intent?) {
        super.onReceive(context, intent)
        when(intent?.action ?: return) {
            ACTION_EXECUTE -> {
                executeCommand(context, intent)
                val index = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1)
                initializeViews(context, index)
            }
            ACTION_INITIALIZE -> {
                val index = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1)
                initializeViews(context, index)
            }
        }
    }

    override fun onDeleted(context: Context, appWidgetIds: IntArray) {
        super.onDeleted(context, appWidgetIds)
        for (id in appWidgetIds) {
            deleteWidget(context, id)
        }
    }

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager?, appWidgetIds: IntArray) {
        super.onUpdate(context, appWidgetManager, appWidgetIds)
        for (index in appWidgetIds) {
            initializeViews(context, index)
        }
    }

    private fun initializeViews(context: Context, index: Int) {
        val remoteViews = RemoteViews(context.packageName, R.layout.widget_run_single_command)
        remoteViews.setOnClickPendingIntent(R.id.run_first_command_button, getPendingSelfIntent(context, index))
        val widgetData = readWidget(context, index) ?: return
        setImage(context, remoteViews, widgetData)
        val appWidgetManager = AppWidgetManager.getInstance(context)
        appWidgetManager.updateAppWidget(index, remoteViews)
    }

    private fun setImage(context: Context, remoteViews: RemoteViews, widgetData: WidgetData)
    {
        if (widgetData.isImg) {
            val uri = Uri.parse(widgetData.img)
            val file = context.contentResolver.openFileDescriptor(uri, "r")!!
            val b = BitmapFactory.decodeFileDescriptor(file.fileDescriptor)
            remoteViews.setImageViewBitmap(R.id.run_first_command_button, b)
        } else {
            val index = widgetData.img.toInt()
            val array = context.resources.obtainTypedArray(R.array.sample_widget_icons)
            val icon = array.getResourceId(index, R.drawable.ic_warning)
            array.recycle()
            remoteViews.setImageViewResource(R.id.run_first_command_button, icon)
        }
    }

    private fun executeCommand(context: Context, intent: Intent) {
        val idx = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1)
        val wd = readWidget(context, idx)
        BackgroundService.RunCommand(context) { service: BackgroundService ->
            try {
                val plugin = service.getDevice(wd!!.deviceId).getPlugin(RunCommandPlugin::class.java)
                if (plugin == null) {
                    openErrorPrompt(context, false)
                    return@RunCommand
                }
                val entry = plugin.commandItems?.find { c -> c.key == wd.commandId }
                if (entry == null) {
                    openErrorPrompt(context, true)
                    return@RunCommand
                }
                plugin.runCommand(entry.key)
            } catch (ex: Exception) {
                Log.e("SingleCommandWidget", "Error running command", ex)
            }
        }
    }

    private fun openErrorPrompt(context: Context, isCommandNotListed: Boolean) {
        val popup = Intent(context, CantRunCommandActivity::class.java)
        popup.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        popup.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
        popup.putExtra(COMMAND_NOT_LISTED, isCommandNotListed)
        context.startActivity(popup)

    }

    private fun getPendingSelfIntent(context: Context, index: Int): PendingIntent? {
        val intent = Intent()
                .setClass(context, SingleCommandWidget::class.java)
                .setAction(ACTION_EXECUTE)
                .putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, index)
        intent.data = Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME))
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_ONE_SHOT)
    }
}